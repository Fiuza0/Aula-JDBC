package br.ucsal.aula.bd.view;

import java.sql.SQLException;
import java.util.List;

import br.ucsal.aula.bd.domain.Correntista;
import br.ucsal.aula.bd.persistence.CorrentistaDAO;
import br.ucsal.aula.bd.persistence.DBUtil;

public class Exemplo {

	public static void main(String[] args) throws SQLException {
		try {
			DBUtil.connect("postgres", "abcd1234");
			listarCorrentistas();
		} finally {
			DBUtil.close();
		}
	}

	private static void listarCorrentistas() throws SQLException {
		List<Correntista> correntistas = CorrentistaDAO.findAll();
		System.out.println("Correntistas: ");
		correntistas.stream().forEach(System.out::println);
	}

}

