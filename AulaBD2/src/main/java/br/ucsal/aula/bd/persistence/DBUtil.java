package br.ucsal.aula.bd.persistence;

import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.SQLException;

public class DBUtil {

	private static Connection connection;
	private static final String URL = "jdbc:postgresql://localhost:62917/aula-jdbc";
	private static final String USER = "postgres";
	private static final String PASSWORD = "postgresql";

	
	private DBUtil(){
		
	}
	public static void connect(String user, String password) throws SQLException {
		connection = DriverManager.getConnection(URL,user,password);
	}
	
	public static Connection getConnection() throws SQLException {
		if(connection == null) {
			connection = DriverManager.getConnection(URL,USER,PASSWORD);
		}
		return connection;
	}
	public static void close() throws SQLException {
		if(connection == null) {
			connection.close();
		}
	}
}
