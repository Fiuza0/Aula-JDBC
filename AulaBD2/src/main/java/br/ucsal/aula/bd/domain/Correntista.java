package br.ucsal.aula.bd.domain;

public class Correntista {

		private Integer id;

		private String nome;

		private String telefone;

		private Integer anoNascimento;

		public Correntista(Integer id, String nome, String telefone, Integer anoNascimento2) {
			super();
			this.id = id;
			this.nome = nome;
			this.telefone = telefone;
			this.anoNascimento = anoNascimento2;
		}

		public Integer getId() {
			return id;
		}

		public void setId(Integer id) {
			this.id = id;
		}

		public String getNome() {
			return nome;
		}

		public void setNome(String nome) {
			this.nome = nome;
		}

		public String getTelefone() {
			return telefone;
		}

		public void setTelefone(String telefone) {
			this.telefone = telefone;
		}

		public Integer getAnoNascimento() {
			return anoNascimento;
		}

		public void setAnoNascimento(Integer anoNascimento) {
			this.anoNascimento = anoNascimento;
		}

		@Override
		public String toString() {
			return "Correntista [id=" + id + ", nome=" + nome + ", telefone=" + telefone + ", anoNascimento="
					+ anoNascimento + "]";
	}

		

}

